// package main

// import (
// 	"fmt"
// 	"os"
// )

// func main() {
// 	// phr := `"Michelle" is super duper awesome!`
// 	// // back ticks make everything printable
// 	// for i, r := range phr {
// 	// 	fmt.Printf("%d %c\n", i, r)
// 	// }

// 	// fmt.Printf("%d\n", len(phr)) // returns length of phrase

// 	n, err := fmt.Printf("Hello, World!")

// 	switch {
// 	case err != nil:
// 		os.Exit(1)
// 	case n == 0:
// 		fmt.Printf("No bytes output")
// 		// fallthrough --- it allows it to keep going to next case
// 	case n != 13:
// 		fmt.Print("Wrong number of characters: %d", n)
// 	default:
// 		fmt.Printf("OK!")
// 	}

// 	fmt.Printf("\n")

// }
