// https://github.com/astaxie/build-web-application-with-golang/blob/master/en/preface.md

// package main

// import (
// 	"fmt"
// )

// const (
// 	// message2 = "Hello Michelle! You are not %d\n" // notice that it's = NOT := because it's const
// 	// answer2  = 48
// 	msg  = "%d %d\n"
// 	ans1 = iota * 2
// 	ans2
// )

// func main() {
// 	// var message string // can set a variable this way
// 	// message := "Hello World! The answer is %d\n" // easily set this way & it figures it out
// 	// answer := 45
// 	// answer += 1
// 	// fmt.Printf(message, answer)
// 	// fmt.Printf(message2, answer2)
// 	fmt.Printf(msg, ans1, ans2)
// }
