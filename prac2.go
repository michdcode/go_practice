// package main

// import "fmt"

// func main() {
// 	// var pie float64 = 3.14 // complex
// 	// pi := float64(3.14)// if you want a specific type
// 	// pi := 3.14
// 	// fmt.Printf("Value: %.2f\n", pi) //.2 tells it 2 digits

// 	// nine := 9
// 	// fmt.Printf("Value: %d\n", nine)

// 	var isTrue1 bool // another way, default is zero value
// 	isTrue := true
// 	fmt.Printf("Value: %t %t\n", isTrue1, isTrue)

// }
