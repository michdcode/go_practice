package main

import (
	"fmt"
)

func main() {
	phr := "An elephant does not belong in a zoo."

	vowels := 0
	consonants := 0
	zees := 0

	for _, r := range phr {
		switch r {
		case 'a', 'e', 'i', 'o', 'u':
			vowels += 1
		case 'z':
			zees += 1
			fallthrough
		default:
			consonants += 1
		}
	}

	fmt.Printf("Vowels: %d; Consonants: %d (Zees: %d)\n", vowels, consonants, zees)
}
